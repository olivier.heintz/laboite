import React from 'react';
import i18n from 'meteor/universe:i18n';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data';
import DoneIcon from '@mui/icons-material/Done';

import AdminUserValidationTable from '../../components/admin/AdminUserValidationTable';

const AdminStructureUsersValidation = ({ loading, users }) => {
  const accept = (user) => {
    const data = { targetUserId: user._id };
    Meteor.call('users.acceptAwaitingStructure', { ...data }, (err) => {
      if (err) {
        msg.error(err.reason || err.message);
      } else msg.success(i18n.__('api.methods.operationSuccessMsg'));
    });
  };

  const columnsFields = ['username', 'lastName', 'firstName', 'emails', 'awaitingStructure'];

  return (
    <AdminUserValidationTable
      title={i18n.__('pages.AdminStructureUsersValidationPage.title')}
      users={users}
      loading={loading}
      columnsFields={columnsFields}
      actions={[
        {
          icon: DoneIcon,
          tooltip: i18n.__('pages.AdminStructureUsersValidationPage.actions.validate'),
          onClick: (event, rowData) => {
            accept(rowData);
          },
        },
      ]}
    />
  );
};

AdminStructureUsersValidation.propTypes = {
  users: PropTypes.arrayOf(PropTypes.any).isRequired,
  loading: PropTypes.bool.isRequired,
};

export default withTracker(() => {
  const subUsers = Meteor.subscribe('users.awaitingForStructure');
  const users = Meteor.users.find({ awaitingStructure: { $ne: null } }).fetch();
  const loading = !subUsers.ready();
  return { loading, users };
})(AdminStructureUsersValidation);
